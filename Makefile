#!/usr/bin/make

MCU		:= atmega168p
TARGET_MCU	:= m168p
CLOCK		:= 8000000UL

PROGRAMMER	:= avrispmkII

PREFIX		:= avr
CC		:= $(PREFIX)-gcc
CXX		:= $(PREFIX)-g++
LD		:= $(PREFIX)-ld
DEBUGGER	:= $(PREFIX)-gdb
SIZE		:= $(PREFIX)-size
READELF		:= $(PREFIX)-readelf
OBJCOPY		:= $(PREFIX)-objcopy
OBJDUMP		:= $(PREFIX)-objdump
WRITE		:= avrdude

buildtype	:= debug

DEBUG_FLAGS	:= -g3 -Os -DDEBUG
RELEASE_FLAGS	:= -Os -DNDEBUG

SUB_DIRECTORY	:= ./common

CFLAGS		:= -std=c99 -Wall -Wextra -Wno-unused-parameter -Wconversion
CFLAGS		+= -mmcu=$(MCU) -DF_CPU=$(CLOCK)
CFLAGS		+= 
CFLAGS		+= -D_GNU_SOURCE

CXXFLAGS	:= -std=c++14 -Wall -Wextra -Wno-unused-parameter -Wconversion
CXXFLAGS	+= -mmcu=$(MCU) -DF_CPU=$(CLOCK)
CXXFLAGS	+= 
CXXFLAGS	+= -D_GNU_SOURCE

SOURCES_DIR	:= .
SOURCES_C	:= $(wildcard $(SOURCES_DIR)/*.c)
SOURCES_C	+= $(wildcard $(SUB_DIRECTORY)/*.c)
SOURCES_CPP	:= $(wildcard $(SOURCES_DIR)/*.cpp)
SOURCES_CPP	+= $(wildcard $(SUB_DIRECTORY)/*.cpp)
SOURCES_CC	:= $(wildcard $(SOURCES_DIR)/*.cc)
SOURCES_CC	+= $(wildcard $(SUB_DIRECTORY)/*.cc)
SOURCES_CXX	:= $(wildcard $(SOURCES_DIR)/*.cxx)
SOURCES_CXX	+= $(wildcard $(SUB_DIRECTORY)/*.cxx)
SOURCES_ASM	:= $(wildcard $(SOURCES_DIR)/*.s)
SOURCES_ASM	+= $(wildcard $(SUB_DIRECTORY)/*.s)


OBJ_DIR		:= ./obj
OBJECTS		:= $(addprefix $(OBJ_DIR)/$(buildtype)/, $(subst ./,,$(patsubst %.c,%.o,$(SOURCES_C))))
OBJECTS		+= $(addprefix $(OBJ_DIR)/$(buildtype)/, $(subst ./,,$(patsubst %.cpp,%.o,$(SOURCES_CPP))))
OBJECTS		+= $(addprefix $(OBJ_DIR)/$(buildtype)/, $(subst ./,,$(patsubst %.cc,%.o,$(SOURCES_CC))))
OBJECTS		+= $(addprefix $(OBJ_DIR)/$(buildtype)/, $(subst ./,,$(patsubst %.cxx,%.o,$(SOURCES_CXX))))
OBJECTS		+= $(addprefix $(OBJ_DIR)/$(buildtype)/, $(subst ./,,$(patsubst %.s,%.o,$(SOURCES_ASM))))

DEP_DIR		:= ./obj
DEPENDS		:= $(addprefix $(DEP_DIR)/$(buildtype)/, $(subst ./,,$(patsubst %.c,%.d,$(SOURCES_C))))
DEPENDS		+= $(addprefix $(DEP_DIR)/$(buildtype)/, $(subst ./,,$(patsubst %.cpp,%.d,$(SOURCES_CPP))))
DEPENDS		+= $(addprefix $(DEP_DIR)/$(buildtype)/, $(subst ./,,$(patsubst %.cc,%.d,$(SOURCES_CC))))
DEPENDS		+= $(addprefix $(DEP_DIR)/$(buildtype)/, $(subst ./,,$(patsubst %.cxx,%.d,$(SOURCES_CXX))))


INCLUDE		:= -I./
INCLUDE		+= -I./$(SUB_DIRECTORY)

LIBS_DIR	:= 
LIBS		:= -lm


TARGET_DIR	:= ./bin
TARGET_NAME	?= target
TARGET_ELF	:= $(TARGET_DIR)/$(buildtype)/$(TARGET_NAME).elf
TARGET_BIN	:= $(TARGET_DIR)/$(buildtype)/$(TARGET_NAME).bin
TARGET_HEX	:= $(TARGET_DIR)/$(buildtype)/$(TARGET_NAME).hex
TARGET_LIST	:= $(TARGET_DIR)/$(buildtype)/$(TARGET_NAME).list
TARGET		:= $(TARGET_ELF)
TARGETS		:= $(TARGET_ELF) $(TARGET_BIN) $(TARGET_HEX) $(TARGET_LIST)

GDB_DEBUG_FILE		:= 


ifeq ($(buildtype), debug)
	CFLAGS += $(DEBUG_FLAGS)
else ifeq ($(buildtype), release)
	CFLAGS += $(RELEASE_FLAGS)
else
	$(echo buildtype must be debug or release)
	$(exit 1)
endif


all : build size

build : $(TARGETS)

run : $(TARGET_HEX)
	$(WRITE) -c $(PROGRAMMER) -p $(TARGET_MCU) -U flash:w:$(TARGET_HEX) -v

write : $(TARGET_HEX)
	$(WRITE) -c $(PROGRAMMER) -p $(TARGET_MCU) -U flash:w:$(TARGET_HEX) -v

flash : $(TARGET_HEX)
	$(WRITE) -c $(PROGRAMMER) -p $(TARGET_MCU) -U flash:w:$(TARGET_HEX) -v

writefuse : $(TARGET_HEX)
	$(WRITE) -c $(PROGRAMMER) -p $(TARGET_MCU) -U lfuse:w:0x62:m -U hfuse:w:0xDF:m -v

-include $(DEPENDS)


$(TARGET_ELF) : $(OBJECTS)
	mkdir -p `dirname $@`
	$(CXX) $^ $(CXXFLAGS) $(INCLUDE) $(LIBS_DIR) $(LIBS) -o $@ -Wl,-M=$(TARGET).map

$(OBJ_DIR)/$(buildtype)/%.o : $(SOURCES_DIR)/%.c Makefile
	mkdir -p `dirname $@`
	$(CC) -c -MMD -MP $< $(CFLAGS) $(INCLUDE) $(LIBS_DIR) $(LIBS) -o $@

$(OBJ_DIR)/$(buildtype)/%.o : $(SOURCES_DIR)/%.cpp Makefile
	mkdir -p `dirname $@`
	$(CXX) -c -MMD -MP $< $(CXXFLAGS) $(INCLUDE) $(LIBS_DIR) $(LIBS) -o $@

$(OBJ_DIR)/$(buildtype)/%.o : $(SOURCES_DIR)/%.cc Makefile
	mkdir -p `dirname $@`
	$(CXX) -c -MMD -MP $< $(CXXFLAGS) $(INCLUDE) $(LIBS_DIR) $(LIBS) -o $@

$(OBJ_DIR)/$(buildtype)/%.o : $(SOURCES_DIR)/%.cxx Makefile
	mkdir -p `dirname $@`
	$(CXX) -c -MMD -MP $< $(CXXFLAGS) $(INCLUDE) $(LIBS_DIR) $(LIBS) -o $@

$(OBJ_DIR)/$(buildtype)/%.o : $(SOURCES_DIR)/%.s Makefile
	mkdir -p `dirname $@`
	$(CC) -c -MMD -MP $< $(CFLAGS) $(INCLUDE) $(LIBS_DIR) $(LIBS) -o $@

$(STARTUP_OBJ) : $(STARTUP_ASM) Makefile
	mkdir -p `dirname $@`
	$(CC) -c $< $(CFLAGS) -o $@

%.hex: %.elf
	$(OBJCOPY) -O ihex $< $@

%.bin: %.elf
	$(OBJCOPY) -O binary $< $@

%.list: %.elf
	$(OBJDUMP) -x -S $< > $@

size: $(TARGET)
	@echo ""
	$(SIZE) $<
	@echo ""

list : $(TARGET_LIST)

clean :
	rm -f -r $(TARGETS) $(OBJECTS) $(DEPENDS) $(TARGET_DIR) $(OBJ_DIR)

.PHONY: all build run write debug size list clean
