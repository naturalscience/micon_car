#include "device.h"

#include <stdint.h>


void init(void);
void control_car(void);


int main(void)
{
	init();

	while (1) {
		//PORTB = (PORTB & LED) ? (PORTB & (uint8_t)~(LED)) : (PORTB | LED);
		PORTB = (PINC & SW_F) ? (PORTB & (uint8_t)~(LED)) : (PORTB | LED);
		control_car();
	}

	return 0;
}


void control_car(void)
{
	if (!(PINC & SW_F) && !(PINC & SW_B)) {
		PORTD |= MOTOR_L_1;
		PORTD |= MOTOR_L_2;
		PORTB |= MOTOR_R_1;
		PORTB |= MOTOR_R_2;
	} else if (!(PINC & SW_F)) {
		if (!(PINC & SW_L)) {
			PORTD &= (uint8_t)~(MOTOR_L_1);
			PORTD &= (uint8_t)~(MOTOR_L_2);
			PORTB |= MOTOR_R_1;
			PORTB &= (uint8_t)~(MOTOR_R_2);
		} else if (!(PINC & SW_R)) {
			PORTD |= MOTOR_L_1;
			PORTD &= (uint8_t)~(MOTOR_L_2);
			PORTB &= (uint8_t)~(MOTOR_R_1);
			PORTB &= (uint8_t)~(MOTOR_R_2);
		} else {
			PORTD |= MOTOR_L_1;
			PORTD &= (uint8_t)~(MOTOR_L_2);
			PORTB |= MOTOR_R_1;
			PORTB &= (uint8_t)~(MOTOR_R_2);
		}
	} else if (!(PINC & SW_B)) {
		if (!(PINC & SW_L)) {
			PORTD &= (uint8_t)~(MOTOR_L_1);
			PORTD &= (uint8_t)~(MOTOR_L_2);
			PORTB &= (uint8_t)~(MOTOR_R_1);
			PORTB |= MOTOR_R_2;
		} else if (!(PINC & SW_R)) {
			PORTD &= (uint8_t)~(MOTOR_L_1);
			PORTD |= MOTOR_L_2;
			PORTB &= (uint8_t)~(MOTOR_R_1);
			PORTB &= (uint8_t)~(MOTOR_R_2);
		} else {
			PORTD &= (uint8_t)~(MOTOR_L_1);
			PORTD |= MOTOR_L_2;
			PORTB &= (uint8_t)~(MOTOR_R_1);
			PORTB |= MOTOR_R_2;
		}
	} else {
		if (!(PINC & SW_L)) {
			PORTD &= (uint8_t)~(MOTOR_L_1);
			PORTD |= MOTOR_L_2;
			PORTB |= MOTOR_R_1;
			PORTB &= (uint8_t)~(MOTOR_R_2);
		} else if (!(PINC & SW_R)) {
			PORTD |= MOTOR_L_1;
			PORTD &= (uint8_t)~(MOTOR_L_2);
			PORTB &= (uint8_t)~(MOTOR_R_1);
			PORTB |= MOTOR_R_2;
		} else {
			PORTB &= (uint8_t)~(LED);
			PORTD &= (uint8_t)~(MOTOR_L_1);
			PORTD &= (uint8_t)~(MOTOR_L_2);
			PORTB &= (uint8_t)~(MOTOR_R_1);
			PORTB &= (uint8_t)~(MOTOR_R_2);
			return;
		}
	}
	PORTB |= LED;
}


void init(void)
{
	DDRB = (1 << DDB0) | (1 << DDB6) | (1 << DDB7);
	DDRC = 0;
	DDRD = (1 << DDD2) | (1 << DDD3);
}
