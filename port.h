#ifndef PORT_H
#define PORT_H

#define SW_F (1 << PORTC5)
#define SW_B (1 << PORTC4)
#define SW_L (1 << PORTC3)
#define SW_R (1 << PORTC2)

#define MOTOR_R_1 (1 << PORTB6)
#define MOTOR_R_2 (1 << PORTB7)

#define MOTOR_L_1 (1 << PORTD2)
#define MOTOR_L_2 (1 << PORTD3)

#define LED (1 << PORTB0)

#endif
